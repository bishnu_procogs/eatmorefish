// JavaScript Document
/*********************/
// Location Search Filter
//<![CDATA[

//]]>
//end ocation Search Filter


jQuery(document).ready(function() {
    //Sticky Menu
    jQuery('#nav_bg').stickit({scope: StickScope.Document, zIndex: 101});

    //Modal open
    jQuery(".modal").on("show", function () {
        jQuery("body").addClass("modal-open");
    }).on("hidden", function () {
        jQuery("body").removeClass("modal-open")
    });
})

// Numpber input
    //jQuery('<div class="quantity-button quantity-up qty_up"><i class="fa fa-angle-up"></i></div><div class="quantity-button quantity-down qty_down"><i class="fa fa-angle-down"></i></div>').insertAfter('.quantity input');
    jQuery('.quantity').each(function() {
        var spinner = jQuery(this),
        input = spinner.find('input[type="number"]'),
        btnUp = spinner.find('.quantity-up'),
        btnDown = spinner.find('.quantity-down'),
        min = input.attr('min'),
        max = input.attr('max');

        btnUp.click(function() {
            var oldValue = parseFloat(input.val());
            if (oldValue >= max) {
            var newVal = oldValue;
            } else {
            var newVal = oldValue + 1;
            }
            spinner.find("input").val(newVal);
            spinner.find("input").trigger("change");
        });

        btnDown.click(function() {
            var oldValue = parseFloat(input.val());
            if (oldValue <= min) {
            var newVal = oldValue;
            } else {
            var newVal = oldValue - 1;
            }
            spinner.find("input").val(newVal);
            spinner.find("input").trigger("change");
        });

    });


// Upload image
    $(function() {
        $('#pro_item').addClass('dragging').removeClass('dragging');
    });
    $('#pro_item').on('dragover', function() {
        $('#pro_item').addClass('dragging')
    }).on('dragleave', function() {
        $('#pro_item').removeClass('dragging')
    }).on('drop', function(e) {
        $('#pro_item').removeClass('dragging hasImage');

        if (e.originalEvent) {
            var file = e.originalEvent.dataTransfer.files[0];
            console.log(file);

            var reader = new FileReader();

            //attach event handlers here...

            reader.readAsDataURL(file);
            reader.onload = function(e) {
                console.log(reader.result);
                $('#pro_item').css('background-image', 'url(' + reader.result + ')').addClass('hasImage');

            }

        }
    })
    $('#pro_item').on('click', function(e) {
        console.log('clicked')
        $('#mediaFile').click();
    });
    window.addEventListener("dragover", function(e) {
        e = e || event;
        e.preventDefault();
    }, false);
    window.addEventListener("drop", function(e) {
        e = e || event;
        e.preventDefault();
    }, false);
    $('#mediaFile').change(function(e) {

        var input = e.target;
        if (input.files && input.files[0]) {
            var file = input.files[0];

            var reader = new FileReader();

            reader.readAsDataURL(file);
            reader.onload = function(e) {
                console.log(reader.result);
                $('#pro_item').css('background-image', 'url(' + reader.result + ')').addClass('hasImage');
            }
        }
    })

//=========================================================================================================



